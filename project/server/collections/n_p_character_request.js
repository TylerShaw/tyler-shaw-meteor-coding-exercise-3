/*
 * Add query methods like this:
 *  NPCharacterRequest.findPublic = function () {
 *    return NPCharacterRequest.find({is_public: true});
 *  }
 */
NPCharacterRequest.allow({
  insert: function (userId, doc) {
    return true;
  },

  update: function (userId, doc, fieldNames, modifier) {
    return true;
  },

  remove: function (userId, doc) {
    return true;
  }
});

NPCharacterRequest.deny({
  insert: function (userId, doc) {
    return false;
  },

  update: function (userId, doc, fieldNames, modifier) {
    return false;
  },

  remove: function (userId, doc) {
    return false;
  }
});