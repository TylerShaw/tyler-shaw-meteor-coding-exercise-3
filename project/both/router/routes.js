/*****************************************************************************/
/* Client and Server Routes */
/*****************************************************************************/
Router.configure({
  layoutTemplate: 'MasterLayout',
  loadingTemplate: 'Loading',
  notFoundTemplate: 'NotFound'
});

Router.route('/', {name: 'home'});
Router.route('/scenarios', {name: 'scenarios'});
Router.route('/npCharacter', {name: 'npCharacter'});
Router.route('/playerRequest', {name: 'playerRequest'});
Router.route('/playerResponse', {name: 'playerResponse'});
Router.route('/npCharacterRequest', {name: 'npCharacterRequest'});
Router.route('/npCharacterResponse', {name: 'npCharacterResponse'});