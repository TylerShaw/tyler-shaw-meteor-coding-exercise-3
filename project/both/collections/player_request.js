PlayerRequest = new Mongo.Collection('player_request');

PlayerRequest.attachSchema(new SimpleSchema({
  title:{
    type:String
  },
  playerHaggle:{
      type:String
  }
}));
/*
 * Add query methods like this:
 *  PlayerRequest.findPublic = function () {
 *    return PlayerRequest.find({is_public: true});
 *  }
 */