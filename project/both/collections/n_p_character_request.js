NPCharacterRequest = new Mongo.Collection('n_p_character_request');

NPCharacterRequest.attachSchema(new SimpleSchema({

  title:{
    type:String
  },
  npCharacterResponseIds:{
    type:[String]
  },
  npCharacterDisposition:{
    type:String
  }

}));